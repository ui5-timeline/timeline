# UI5 timeline control

UI5 timeline control. Displays data in a timeline.

Grouping is done by month. Each day is then shown as a card, containing a title and text.

# Usage

The UI5 timeline control is a custom UI5 control. To use it in your UI5 app, follow the standard. Put the custom control in folder de/itsfullofstars/time under libs in your webapp folder.

Load the library in manifest.json. In section sap.ui5, add:

```json
"resourceRoots": {
    "de.itsfullofstars.timeline": "./libs/de/itsfullofstars/timeline"
},
```

In a view, declare the namespace:

```xml
xmlns:tl="de.itsfullofstars.timeline.controls"
```

Use the control in the view:

```xml
<tl:Timeline
    title="Test Title"
    items="<model>"
    >
    <tl:items>
    </tl:items>
</tl:Timeline>
```

# Notes

Timeline takes inspiration and CSS from https://codepen.io/melnik909/pen/qPjwvq
